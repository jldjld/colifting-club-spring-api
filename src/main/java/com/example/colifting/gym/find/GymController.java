package com.example.colifting.gym.find;

import com.example.colifting.gym.Gym;
import com.example.colifting.gym.GymRepositoryInterface;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
@RequestMapping("/api/v1/gyms")
public class GymController {

    private final GymRepositoryInterface repository;

    public GymController(GymRepositoryInterface repository) {
        this.repository = repository;
    }

    /**
     * Search Gyms by city endpoint
     * @param city where we want to find gyms
     * @return Gym
     * @throws ResponseStatusException if gym is not found
     */
    @GetMapping("/location/{city}")
    public ResponseEntity<Set<Gym>> getGymsByCity(String city) throws ResponseStatusException {
        // Verify if numbers or special characters exist in $city
        String regex    = "[0-9\\W]";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(city);

        if (matcher.find()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }

        city = city.toLowerCase();
        Set<Gym> foundGyms  = this.repository.findByCity(city);
        if(foundGyms.isEmpty()) {
            throw new  ResponseStatusException(HttpStatus.NOT_FOUND, "Gyms not found for this location.");
        }

        return ResponseEntity.ok(foundGyms);
    }
}
