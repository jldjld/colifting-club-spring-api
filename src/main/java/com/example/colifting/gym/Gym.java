package com.example.colifting.gym;

import com.example.colifting.availability.Availability;
import com.example.colifting.club.Club;
import com.example.colifting.gymUser.GymUser;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;
import java.util.UUID;

@Entity
@Table(name="GYM")
@Getter
@Setter
public class Gym {
    @Id
    @GeneratedValue
    @NotNull
    private UUID id = UUID.randomUUID();

    @Column(name="NAME", length=50, nullable=false)
    private String name;

    @Column(name="CITY", length=50, nullable=false)
    private String city;

    @OneToMany(mappedBy="mainGym")
    private Set<GymUser> members;

    @ManyToOne
    @JoinColumn(name="CLUB_ID")
    private Club club;

    @Column(name="DESCRIPTION", length=250)
    private String description;

    @Column(name="PHONE")
    private String phone;

    @Column(name="OWNER", unique = true)
    private GymUser owner;

    @ManyToMany
    @JoinTable(
        name = "gym_availability",
        joinColumns = @JoinColumn(name = "gym_id"),
        inverseJoinColumns = @JoinColumn(name = "availability_id")
    )
    private Set<Availability> availability;
}
