package com.example.colifting.gym;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Set;
import java.util.UUID;

public interface GymRepositoryInterface extends CrudRepository<Gym, UUID> {

    Gym findByName(String name);

    @Query("SELECT g FROM Gym g WHERE g.city LIKE :city")
    Set<Gym> findByCity(@Param("city") String city);
}