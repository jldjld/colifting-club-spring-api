package com.example.colifting.gymUser;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Set;
import java.util.UUID;

public interface GymUserRepositoryInterface extends CrudRepository<GymUser, UUID> {

    GymUser findByEmail(String email);

    GymUser findByUsername(String username);

    @Query("SELECT g FROM GymUser g WHERE g.city = :city AND g.club.id = :clubId")
    Set<GymUser> findByCityAndClub(@Param("city") String city, @Param("clubId") UUID clubId);
}
