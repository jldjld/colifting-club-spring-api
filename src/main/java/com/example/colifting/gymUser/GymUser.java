package com.example.colifting.gymUser;

import com.example.colifting.availability.Availability;
import com.example.colifting.club.Club;
import com.example.colifting.gym.Gym;
import com.example.colifting.gymUser.userEnum.UserGoals;
import com.example.colifting.gymUser.userEnum.UserRole;
import com.example.colifting.userInvitation.UserInvitation;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name="GYMUSER")
@Getter
@Setter
public class GymUser implements UserDetails {

    private GymUser gymUser;

    public GymUser() {
    }

    public GymUser(GymUser user) {
        this.gymUser = user;
    }

    @Id
    @GeneratedValue
    @NotNull
    private UUID id = UUID.randomUUID();

    @Column(name="EMAIL", length=50, nullable=false)
    private String email;

    @Column(name="PASSWORD", length=50, nullable=false )
    private String password;

    @Column(name="USERNAME", length=50, nullable=false)
    private String username;

    @Column(name="ROLE", length=50, nullable=false)
    private UserRole roles;

    @Column(name="GENDER", length=1)
    private String gender;

    @ElementCollection(targetClass = UserGoals.class)
    @CollectionTable(name="USER_GOALS", joinColumns=@JoinColumn(name = "GYMUSER_ID"))
    @Enumerated(EnumType.STRING)
    private Set<UserGoals> goals;

    @ManyToMany
    @JoinTable(
        name = "gymuser_availability",
        joinColumns = @JoinColumn(name = "gymuser_id"),
        inverseJoinColumns = @JoinColumn(name = "availability_id")
    )
    private Set<Availability> availability;

    @OneToMany(mappedBy="user")
    private Set<UserInvitation> invitations;

    @Column(name="CITY")
    private String city;

    @ManyToOne
    @JoinColumn(name="GYM_ID")
    private Gym mainGym = null;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name="CLUB_ID")
    private Club club = null;

    @Column(name="IS_VERIFIED", nullable=false)
    private Boolean isVerified = false;

    @Column(name="IS_FIRST_LOGIN", nullable=false)
    private Boolean isFirstLogin = true;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public String getUsername() {
        return null;
    }

    @Override
    public boolean isAccountNonExpired() {
        return false;
    }

    @Override
    public boolean isAccountNonLocked() {
        return false;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return false;
    }

    @Override
    public boolean isEnabled() {
        return this.isVerified;
    }
}