package com.example.colifting.gymUser.userEnum;
public enum UserRole {
    USER, ADMIN, MANAGER
}