package com.example.colifting.gymUser.userEnum;

public enum UserGoals {
    STRENGTH, FAT_LOSS, GAIN_MUSCLE, ENDURANCE, FLEXIBILITY, TONE_UP
}