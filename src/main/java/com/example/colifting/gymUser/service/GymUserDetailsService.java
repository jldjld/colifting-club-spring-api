package com.example.colifting.gymUser.service;

import com.example.colifting.gymUser.GymUser;
import com.example.colifting.gymUser.GymUserRepositoryInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class GymUserDetailsService implements UserDetailsService {

    @Autowired
    private GymUserRepositoryInterface repository;

    @Override
    public UserDetails loadUserByUsername(String email) {
        GymUser user = repository.findByEmail(email);
        if (user == null) {
            throw new UsernameNotFoundException(email);
        }
        return new GymUser(user);
    }

}
