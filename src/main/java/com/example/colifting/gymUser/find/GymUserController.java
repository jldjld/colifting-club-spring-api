package com.example.colifting.gymUser.find;

import com.example.colifting.gymUser.GymUser;
import com.example.colifting.gymUser.GymUserRepositoryInterface;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.Set;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/user")
public class GymUserController {

    private final GymUserRepositoryInterface repository;

    public GymUserController(GymUserRepositoryInterface repository) {
        this.repository = repository;
    }

    /**
     * Find GymUser by id
     *
     * @param gymUserId id of the user
     * @return GymUser
     * @throws ResponseStatusException if user is not found
     */
    @GetMapping("/{gymUserId}")
    public ResponseEntity<GymUser> findGymUserById(@PathVariable UUID gymUserId) throws ResponseStatusException {
        GymUser gymUser = this.repository.findById(gymUserId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "GymUserNot found."));

        return ResponseEntity.ok(gymUser);
    }

    /**
     * Find gymUser by city and club
     *
     * @param city where we want to find users
     * @param clubId club id where we want to find users
     * @return List<GymUser>
     * @throws ResponseStatusException if no users are found
     */
    public ResponseEntity<Set<GymUser>> findByCityAndClub(String city, UUID clubId ) throws ResponseStatusException {
        Set<GymUser> foundUsers = this.repository.findByCityAndClub( city, clubId );
        if(foundUsers.isEmpty()) {
            throw new  ResponseStatusException(HttpStatus.NOT_FOUND, "Users not found is this club.");
        }

        return ResponseEntity.ok(foundUsers);
    }
}
