package com.example.colifting.availability;

import com.example.colifting.availability.availabilityEnum.TimeSlot;
import com.example.colifting.gym.Gym;
import com.example.colifting.gymUser.GymUser;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name="AVAILABILITY")
@Getter
@Setter
public class Availability {
    @Id
    @GeneratedValue
    @NotNull
    private UUID id = UUID.randomUUID();

    @Column(name="DAYS", length=50, nullable=false)
    private ArrayList<DayOfWeek> days;

    @Column(name="TIMESLOT", length=50, nullable=false)
    private ArrayList<TimeSlot> timeslot;

    @ManyToMany(mappedBy="availability")
    private Set<GymUser> members;

    @ManyToMany(mappedBy="availability")
    private Set<Gym> gym;
}
