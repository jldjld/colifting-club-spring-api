package com.example.colifting.club;

import com.example.colifting.gym.Gym;
import com.example.colifting.gymUser.GymUser;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;
import java.util.UUID;

@Entity
@Table(name="CLUB")
@Getter
@Setter
public class Club {

    @Id
    @GeneratedValue
    @NotNull
    private UUID id = UUID.randomUUID();

    @Column(name="NAME", length=50, nullable=false, unique=true)
    private String name;

    @Column(name="DESCRIPTION", length=250)
    private String description;

    @OneToMany(mappedBy="club")
    private Set<GymUser> members;

    @OneToMany(mappedBy="club")
    private Set<Gym> gyms;
}
