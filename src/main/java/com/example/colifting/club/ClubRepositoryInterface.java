package com.example.colifting.club;

import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

public interface ClubRepositoryInterface extends CrudRepository<Club, UUID> {

        Club findByName(String name);
}
