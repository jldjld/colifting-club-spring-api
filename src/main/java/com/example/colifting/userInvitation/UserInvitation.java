package com.example.colifting.userInvitation;

import com.example.colifting.gymUser.GymUser;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;
import java.util.UUID;

@Entity
@Table(name="USER_INVITATION")
@Getter
@Setter
public class UserInvitation {
    @Id
    @GeneratedValue
    @NotNull
    private UUID id = UUID.randomUUID();

    @Column(name="ACCEPTED", nullable=true)
    private Set<UUID> accepted;

    @Column(name="DECLINED", nullable=true)
    private Set<UUID> declined;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private GymUser user = null;

    @Column(name="PENDING", nullable=true)
    private Set<UUID> pending;
}
