package com.example.colifting;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ColiftingApplication {

	public static void main(String[] args) {
		SpringApplication.run(ColiftingApplication.class, args);
	}

}
