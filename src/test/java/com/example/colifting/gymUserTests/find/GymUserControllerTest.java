package com.example.colifting.gymUserTests.find;

import com.example.colifting.club.Club;
import com.example.colifting.club.ClubRepositoryInterface;
import com.example.colifting.gymUser.GymUser;
import com.example.colifting.gymUser.GymUserRepositoryInterface;
import com.example.colifting.gymUser.find.GymUserController;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.http.ResponseEntity;
import org.springframework.web.server.ResponseStatusException;

import java.util.*;

import static org.mockito.Mockito.*;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class GymUserControllerTest {

    private GymUserController gymUserController;

    @Autowired
    private ApplicationContext applicationContext;

    private GymUserRepositoryInterface gymUserRepository;

    GymUser gymUserItem = new GymUser();

    Set<GymUser> members = new HashSet<>();

    Club clubItem = new Club();

    @BeforeEach
    void setUp() {
        // Mock gymUserRepository
        gymUserRepository = mock(GymUserRepositoryInterface.class);
        gymUserController = new GymUserController(gymUserRepository);

        // Mock clubRepository
        ClubRepositoryInterface clubRepository = mock(ClubRepositoryInterface.class);

        // add a club
        clubItem.setName("Club de test");
        clubRepository.save(clubItem);

        // add a gymUser
        gymUserItem.setEmail("toto@test.fr");
        gymUserItem.setPassword("tototo1");
        gymUserItem.setCity("Nantes");
        gymUserItem.setClub(clubItem);
        gymUserRepository.save(gymUserItem);

        // Init members Set list
        members.add(gymUserItem);

        clubItem.setMembers(members);
        clubRepository.save(clubItem);
    }

    @Test
    void apiGymUserByIdOk() {
        when(gymUserRepository.findById(gymUserItem.getId())).thenReturn(Optional.of(gymUserItem));

        ResponseEntity<GymUser> userItem = gymUserController.findGymUserById( gymUserItem.getId() );
        // Verify that the method was called once and status code is 200
        Mockito.verify(gymUserRepository, Mockito.times(1)).findById(gymUserItem.getId());
        Assertions.assertEquals(userItem.getStatusCode(), ResponseEntity.ok().build().getStatusCode());
        Assertions.assertEquals(userItem.getBody(), gymUserItem);
    }

    @Test
    void apiGymUserByIdKo() {
        UUID uuid = UUID.fromString("018cc9e7-9245-7dd6-97be-c2f05b0022b2");
        when(gymUserRepository.findById(uuid)).thenReturn(Optional.empty());

        Assertions.assertThrows(ResponseStatusException.class, () -> {
            gymUserController.findGymUserById(uuid);
        });
    }

    @Test
    void apiGymUserByCityAndClubOk() {
        when(gymUserRepository.findByCityAndClub( "Nantes", clubItem.getId())).thenReturn(members);

        ResponseEntity<Set<GymUser>> response = gymUserController.findByCityAndClub("Nantes", clubItem.getId());
        // Verify that the method was called once and status code is 200
        Mockito.verify(gymUserRepository, Mockito.times(1)).findByCityAndClub("Nantes", clubItem.getId());
        Assertions.assertEquals(response.getStatusCode(), ResponseEntity.ok().build().getStatusCode());
        Assertions.assertEquals(response.getBody(), members);
    }

    @Test
    void apiGymUserByCityAndClubKo() {
        when(gymUserRepository.findByCityAndClub( "Pau", clubItem.getId())).thenReturn(Collections.emptySet());

        Assertions.assertThrows(ResponseStatusException.class, () -> {
            gymUserController.findByCityAndClub("Pau", clubItem.getId());
        });
    }
}
