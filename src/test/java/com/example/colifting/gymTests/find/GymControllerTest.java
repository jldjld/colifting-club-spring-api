package com.example.colifting.gymTests.find;

import com.example.colifting.club.Club;
import com.example.colifting.club.ClubRepositoryInterface;
import com.example.colifting.gym.Gym;
import com.example.colifting.gym.GymRepositoryInterface;
import com.example.colifting.gym.find.GymController;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.ApplicationContext;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

@WebMvcTest(GymController.class)
public class GymControllerTest {

    private GymController gymController;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ApplicationContext applicationContext;

    @MockBean
    private GymRepositoryInterface gymRepository;

    Gym gymItem = new Gym();

    Set<Gym> gyms = new HashSet<>();

    Club clubItem = new Club();

    @BeforeEach
    void setUp() {
        // Mock gymRepository
        gymRepository = mock(GymRepositoryInterface.class);
        gymController = new GymController(gymRepository);

        // Mock clubRepository
        ClubRepositoryInterface clubRepository = mock(ClubRepositoryInterface.class);

        when(clubRepository.save(any(Club.class))).thenAnswer(i -> i.getArguments()[0]);
        when(gymRepository.save(any(Gym.class))).thenAnswer(i -> i.getArguments()[0]);
        // add a club
        clubItem.setName("Club de test");
        clubRepository.save(clubItem);

        // add a gym
        gymItem.setName("Gym de test");
        gymItem.setCity("Nantes");
        gymItem.setClub(clubItem);
        gymRepository.save(gymItem);

        gyms.add(gymItem);

        clubItem.setGyms(gyms);
        clubRepository.save(clubItem);
    }

    @Test
    void apiGetGymsByCityOk() throws Exception {
        //when(gymRepository.findByCity( "Nantes")).thenReturn(gyms);

        when(gymRepository.findByCity("Nantes")).thenReturn(gyms);
        this.mockMvc.perform(get("/api/v1/gyms/location/Nantes")).andExpect(status().isOk());
                //.andExpect(content().string(containsString("Hello, Mock")));

        //Mockito.verify(gymRepository, Mockito.times(1)).findByCity("Nantes");
        // Verify that the method was called once and status code is 200
        //Assertions.assertEquals(response.getBody(), gyms);
        //Assertions.assertEquals(response.getStatusCode(), ResponseEntity.ok().build().getStatusCode());
    }
}
